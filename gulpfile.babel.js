import gulp from 'gulp';
import loadPlugins from 'gulp-load-plugins';
// import webpack from 'webpack';
import rimraf from 'rimraf';

// const plugins = loadPlugins();

// import testpageWebpackConfig from './webpack.config';

// gulp.task('testpage-js', ['clean'], (cb) => {
//   // webpack(testpageWebpackConfig, (err, stats) => {
//   //   if(err) throw new plugins.util.PluginError('webpack', err);
//   //
//   //   plugins.util.log('[webpack]', stats.toString());
//   //
//   //   cb();
//   // });
// });

gulp.task('copy-assets', ['clean'], () => {
  return gulp.src(['assets/**/*']).pipe(gulp.dest('./build/assets'));
});

gulp.task('copy-src', ['clean'], () => {
  return gulp.src(['src/**/*']).pipe(gulp.dest('./build'));
});

gulp.task('clean', (cb) => {
  rimraf('./build', cb);
});


gulp.task('build', ['copy-assets', 'copy-src']);

gulp.task('watch', ['default'], () => {
  gulp.watch('assets/**/*', ['build']);
  gulp.watch('src/**/*', ['build']);
});

gulp.task('default', ['build']);
